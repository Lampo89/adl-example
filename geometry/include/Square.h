#ifndef SQUARE_H
#define SQUARE_H

namespace geometry
{

/**
 * @brief 
 * 
 */
class Square
{
public:
    /**
     * @brief Construct a new Square object
     * 
     * @param side 
     */
    explicit Square(double side);

    /**
     * @brief Get the Side object
     * 
     * @return double 
     */
    double getSide() const;

private:
    double m_side;
};

/**
 * @brief Get the Perimeter object
 * 
 * @param c 
 * @return double 
 */
double getPerimeter(const Square & c);

/**
 * @brief Get the Area object
 * 
 * @param c 
 * @return double 
 */
double getArea(const Square & c);

};


#endif // SQUARE_H
