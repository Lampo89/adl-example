#ifndef CIRCLE_H
#define CIRCLE_H

namespace geometry
{

/**
 * @brief 
 */
class Circle
{
public:
    /**
     * @brief Construct a new Circle object
     * 
     * @param radius 
     */
    explicit Circle(double radius);

    /**
     * @brief Get the Radius object
     * 
     * @return double 
     */
    double getRadius() const;
private:
    double m_radius;
};

/**
 * @brief Get the Perimeter object
 * 
 * @param c 
 * @return double 
 */
double getPerimeter(const Circle & c);

/**
 * @brief Get the Area object
 * 
 * @param c 
 * @return double 
 */
double getArea(const Circle & c);

};

#endif // CIRCLE_H
