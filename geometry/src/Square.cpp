#include "Square.h"

namespace geometry {

Square::Square(double side) : m_side {side}
{
}

double Square::getSide() const
{
    return m_side;
}

double getPerimeter(const Square & c)
{
    return 4 * c.getSide();
}

double getArea(const Square & c)
{
    return c.getSide() * c.getSide();
}

};