#include "Circle.h"

#include <cmath>

namespace geometry {

Circle::Circle(double radius) : m_radius {radius}
{
}

double Circle::getRadius() const { return m_radius; }

double getArea(const Circle & c)
{
    return M_PI * c.getRadius() * c.getRadius(); 
}

double getPerimeter(const Circle & c)
{
    return 2 * M_PI * c.getRadius();
}

};