#include <iostream>

#include "Circle.h"
#include "Square.h"

using namespace std;

template<typename Shape>
double computeArea(const Shape & shape)
{
    return getArea(shape);
}

template<typename Shape>
double computePerimeter(const Shape & shape)
{
    return getPerimeter(shape);
}

int main(int, char**)
{
    geometry::Circle c {2.0};
    geometry::Square s {5.0};

    cout << "Circle,  area: " << computeArea(c) << ", 2p = " << computePerimeter(c) << endl;
    cout << "Square,  area: " << computeArea(s) << ", 2p = " << computePerimeter(s) << endl;
}
